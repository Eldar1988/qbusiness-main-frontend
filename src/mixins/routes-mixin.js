export default {
  data () {
    return {
      links: [
        { label: 'Мой офис', name: 'home', icon: 'las la-briefcase' },
        { label: 'Мой профиль', name: 'profile', icon: 'las la-user' },
        { label: 'Мои заказы', name: 'orders', icon: 'las la-shopping-cart' },
        { label: 'Мои товары', name: 'products', icon: 'las la-store-alt' },
        { label: 'Мои клиенты', name: 'clients', icon: 'las la-id-card' },
        { label: 'Мои заметки', name: 'notes', icon: 'las la-book' }
      ]
    }
  }
}
