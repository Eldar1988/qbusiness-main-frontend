
export default {
  serverUri: 'http://192.168.0.155:1337',
  headers: {
    Authorization: ''
  },

  setJwt (jwt) {
    this.headers.Authorization = `Bearer ${jwt}`
  }
}
