import { Notify } from 'quasar'

// interface Notice {
//   message: string,
//   color: 'primary' | 'secondary' | 'dark' | 'negative'
//   position: 'left' | 'right' | 'top' | 'bottom'
// }

export default function notifier ({ color = 'dark', position = 'top', message = '' }: any) {
  Notify.create({
    message,
    color,
    position,
    actions: [{ color: 'white', icon: 'close' }]
  })
}
