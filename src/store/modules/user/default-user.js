export default {
  id: null,
  username: '',
  email: '',
  provider: '',
  blocked: false,
  orders: [],
  notes: [],
  photo: {
    url: ''
  }
}
