import defaultUser from './default-user'

export default function () {
  return {
    user: defaultUser
  }
}
