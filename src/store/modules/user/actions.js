import axios from 'axios'
import errorHandler from 'src/utils/errorHandler'
import config from 'src/config'
import notifier from 'src/utils/notifier'

export const getMe = async ({ commit }, token) => {
  try {
    await axios
      .get(`${config.serverUri}/users/me`, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
      .then(res => {
        commit('mutationUser', res.data)
        config.setJwt(token)
      })
  } catch (e) {
    errorHandler(e)
  }
}

export const register = async ({ commit }) => {

}

export async function login ({ commit }, payload) {
  const vm = this
  try {
    await axios
      .post(`${config.serverUri}/auth/local`, {
        ...payload
      })
      .then(res => {
        if (res.status === 200) {
          commit('mutationUser', res.data.user)
          const jwt = res.data.jwt
          localStorage.setItem('token', jwt)
          config.setJwt(jwt)
          notifier({ message: 'Авторизация прошла успешно.', color: 'positive' })
          return vm.$router.push({
            name: 'home'
          })
        } else {
          notifier({ message: 'test' })
        }
      })
  } catch (err) {
    errorHandler(err)
  }
}
