import defaultUser from './default-user'
import config from 'src/config'

export const mutationUser = (state, user) => {
  state.user = user
}

export const setDefaultUser = (state) => {
  state.user = defaultUser
  config.setJwt('')
  localStorage.setItem('token', '')
}
