
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'home', meta: { title: 'Мой офис' }, component: () => import('pages/Index.vue') }
    ]
  },

  {
    path: '/profile',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'profile', meta: { title: 'Мой профиль' }, component: () => import('pages/Profile/Profile') }
    ]
  },

  {
    path: '/orders',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'orders', meta: { title: 'Мои заказы' }, component: () => import('pages/Orders/Orders') }
    ]
  },

  {
    path: '/products',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'products', meta: { title: 'Мои товары' }, component: () => import('pages/Products/Products') }
    ]
  },

  {
    path: '/clients',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'clients', meta: { title: 'Мои клиенты' }, component: () => import('pages/Clients/Clients') }
    ]
  },

  {
    path: '/notes',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'notes', meta: { title: 'Мои заметки' }, component: () => import('pages/Notes/Notes') }
    ]
  },

  {
    path: '/auth',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'auth', meta: { title: 'Авторизация' }, component: () => import('pages/Auth/Auth') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
